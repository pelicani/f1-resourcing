<?php

/**
* The style plugin for serialized output formats.
*
* @ingroup views_style_plugins
*
* @ViewsStyle(
*   id = "serializer_date",
*   title = @Translation("Serializer Date"),
*   help = @Translation("Serializes results and groups them by date."),
*   display_types = {"data"}
* )
*/
class SerializerDate extends Serializer {

  /**
  * {@inheritdoc}
  */
  public function render() {
    $rows = [];

    foreach ($this->view->result as $row) {
      $entity = $this->view->rowPlugin->render($row);
      $title = $entity->label();
      $first_letter = Unicode::strtoupper($title[0]);
      $rows[$first_letter][] = $this->view->rowPlugin->render($row);
    }

    // Get the content type configured in the display or fallback to the
    // default.
    if ((empty($this->view->live_preview))) {
      $content_type = $this->displayHandler->getContentType();
    }
    else {
      $content_type = !empty($this->options['formats']) ? reset($this->options['formats']) : 'json';
    }

    return $this->serializer->serialize(['results' => ['items' => $rows, 'totalCount' => count($this->view->result)]], $content_type);
  }
}
