<?php
/**
 * @file
 * Contains \Drupal\resource_api\Controller\ResourceAPIBatchController.
 */
namespace Drupal\resourcing\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ResourcingAPIController
 * @package Drupal\resourcing\Controller
 */
class ResourcingAPIController {

  /**
   * @return null|\Symfony\Component\HttpFoundation\RedirectResponse
   */

  public function dashboard(Request $request) {
    $account = _process_user_account([]);
    return [
      '#theme' => 'resourcing_dashboard',
      '#date' => time(),
      '#user' => $account->getAccountName(),
      '#category' => 'dude',
      '#attached' => [
        'library' => [
          'resourcing/vuerouter',
          'resourcing/vuejs',
          'resourcing/resourcing',
        ],
      ]
    ];
  }
  public function lii(Request $request) {
    $query_params = \Drupal::request()->query->all();
    $account = _process_user_account([]);
    $selected_year = date('Y',time());
    if (!empty($query_params['year'])) {
      $selected_year = $query_params['year'];
    }
    return [
      '#theme' => 'resourcing_lii',
      '#selected_year' => $selected_year,
      '#user' => $account->getAccountName(),
      '#attached' => [
        'library' => [
          'resourcing/vuerouter',
          'resourcing/vuejs',
          'resourcing/vuedraggable',
          'resourcing/sortable',
          'resourcing/lii',
        ],
      ]
    ];
  }
  public function heatmap(Request $request) {
    $query_params = \Drupal::request()->query->all();
    $account = _process_user_account([]);
    $selected_year = date('Y',time());
    $module_handler = \Drupal::service('module_handler');
    $current_path = $module_handler->getModule('resourcing')->getPath();
    if (!empty($query_params['year'])) {
      $selected_year = $query_params['year'];
    }
    return [
      '#theme' => 'resourcing_heatmap',
      '#selected_year' => $selected_year,
      '#user' => $account->getAccountName(),
      '#uid' => $account->id(),
      '#current_path' => $current_path,
      '#attached' => [
        'library' => [
          'resourcing/vuejs',
          'resourcing/user-list',
        ],
      ]
    ];
  }
  public function reporting(Request $request) {
    $query_params = \Drupal::request()->query->all();
    $account = _process_user_account([]);
    $selected_year = date('Y',time());
    if (!empty($query_params['year'])) {
      $selected_year = $query_params['year'];
    }
    return [
      '#theme' => 'resourcing_reporting',
      '#selected_year' => $selected_year,
      '#user' => $account->getAccountName(),
      '#user_id' => $account->id(),
      '#attached' => [
        'library' => [
          'resourcing/vue-calendar-heatmap',
          'resourcing/vue-resize',
          'resourcing/vuerouter',
          'resourcing/vuejs',
          'resourcing/reporting',
        ],
      ]
    ];
  }
  public function routing(Request $request) {
    $account = _process_user_account([]);
    return [
      '#theme' => 'resourcing_routing',
    ];
  }

  public function heat_map_data(Request $request) {
    $account = _process_user_account([]);
    $database = \Drupal::database();
    $query = $database->query("SELECT node__field_date.field_date_value AS date, SUM(node__field_duration.field_duration_value) AS count
      FROM
      {node_field_data} node_field_data
      LEFT JOIN {node__field_date} node__field_date ON node_field_data.nid = node__field_date.entity_id AND node__field_date.deleted = '0'
      LEFT JOIN {node__field_duration} node__field_duration ON node_field_data.nid = node__field_duration.entity_id AND node__field_duration.deleted = '0'
      LEFT JOIN {node__field_status} node__field_status ON node_field_data.nid = node__field_status.entity_id AND node__field_status.deleted = '0'
      WHERE ((node_field_data.uid = '".$account->id()."')) AND ((node_field_data.status = '1') AND (node_field_data.type IN ('card'))AND (node__field_status.field_status_value IN ('active','complete')))
      GROUP BY date
      ORDER BY date DESC");
    $result = $query->fetchAll();
    $json = new JsonResponse($result);
    return $json;
  }
}
