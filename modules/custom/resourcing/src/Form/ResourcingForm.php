<?php
/**
 * @file
 * Contains \Drupal\resourcing\Form\ResourcingForm.
 */

namespace Drupal\resourcing\Form;

use Drupal\node\Entity\Node;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger;
use Drupal\Core\Link;
use Drupal\Core\Url;

class ResourcingForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'resourcing_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $account = _process_user_account($form);
    //get action argument
    $action = \Drupal::request()->query->get('action');
    $export_value = "";

    switch($action) {
      case 'default':
        $export_value = \Drupal::config('resourcing.settings')->get('resource_default');
        break;
      case 'export':
        $uid = $account->id();
        $date = \Drupal::request()->query->get('date');
        if (empty($date)) {
          $date = date('Y-m-d', time());
        }

        \Drupal::messenger()->addStatus('Copy Resourcing from '.  $date.". Please pick a new date, it's required.");

        $database = \Drupal::database();
        $query = $database->select('node_field_data', 'n');
        $query->leftJoin('node__field_date', 'nfd', 'n.nid = nfd.entity_id');
        $query->leftJoin('node__field_duration', 'nfdur', 'n.nid = nfdur.entity_id');
        $query->leftJoin('draggableviews_structure', 'ds', 'n.nid = ds.entity_id');
        $query->leftJoin('node__field_tags', 'nft', 'n.nid = nft.entity_id');
        //LEFT JOIN {node__field_tags} node__field_tags ON node_field_data.nid = node__field_tags.entity_id AND (node__field_tags.deleted = '0' AND node__field_tags.langcode = node_field_data.langcode)
        $query->fields('n', ['title','uid'])
          ->fields('nft', ['field_tags_target_id'])
          ->fields('nfd', ['field_date_value'])
        ->fields('nfdur', ['field_duration_value'])
        ->condition('n.status', 1, '=')
        ->condition('n.type', 'card', '=')
        ->condition('n.uid', $uid, '=')
        ->condition('nfd.field_date_value', $date, '=')
        ->orderBy('nfd.field_date_value','DESC')
        ->orderBy('ds.weight','ASC')
        ->orderBy('n.created','ASC');
        $cards = $query->execute();
        //SELECT node__field_date.field_date_value AS node__field_date_field_date_value, draggableviews_structure.weight AS draggableviews_structure_weight, node_field_data.created AS node_field_data_created, node_field_data.nid AS nid

        //LEFT JOIN {node__field_date} node__field_date ON node_field_data.nid = node__field_date.entity_id AND node__field_date.deleted = '0'
        //LEFT JOIN {draggableviews_structure} draggableviews_structure ON node_field_data.nid = draggableviews_structure.entity_id
        //WHERE (node_field_data.status = '1') AND (node_field_data.type IN ('card')) AND (node_field_data.uid IN ('3'))
        //ORDER BY node__field_date_field_date_value DESC, draggableviews_structure_weight ASC, node_field_data_created ASC

        /*
        $query = \Drupal::entityQuery('node')
          ->condition('type', 'card')
          ->condition('field_date', $date)
          ->condition('uid', $uid)
          ->execute();
        $cards = Node::loadMultiple($query);
        */
        foreach($cards AS $key => $card) {
          $options[] = $card->title.":".$card->field_duration_value.":todo:".$card->field_tags_target_id;
        }

        if (!empty($options)) {
          $export_value = implode("\r\n",$options);
        }
        //sort by drag, then created
        break;
      default:
        break;

    }


    $form['author'] = array(
      '#type' => 'hidden',
      '#title' => t('Author'),
      '#default_value' => $uid
    );
    $query = ['action'=>'default'];
    $url = Url::fromRoute('resourcing.form',$query);
    $internal_link = \Drupal::l(t('Load Default Resourcing'), $url, $options);
    $form['resource_items'] = array(
      '#type' => 'textarea',
      '#title' => t('Resource Items'),
      '#required' => TRUE,
      '#description' => 'link to add default '.$internal_link,
      '#default_value' => $export_value
    );

    $form['resource_date'] = array (
      '#type' => 'date',
      '#title' => t('Resource Date'),
      '#required' => TRUE,
    );
    $form['resource_default'] = array(
      '#type' => 'checkbox',
      '#title' => $this
        ->t('Save as Default'),
    );

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
    public function validateForm(array &$form, FormStateInterface $form_state) {

      \Drupal::messenger()->addStatus('Validate Form');

    }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $account = _process_user_account($form);
    $resource_items = "";
    $field_date = time();
    \Drupal::messenger()->addStatus($this->t('Your resourcing request is being submitted for @res_date!', array('@res_date' => $form_state->getValue('resource_date'))));
    $complete_nodes = [];
    foreach ($form_state->getValues() as $key => $value) {
      //op
      switch ($key) {
        case "resource_default":
          if (!empty($value)) {
            $resource_items = $form_state->getValue('resource_items');
            //$resource_default = \Drupal::config('resourcing.settings')->set('resource_default', $resource_items);
            $resource_default = \Drupal::configFactory()->getEditable('resourcing.settings')
              ->set('resource_default', $resource_items)
              ->save();
            $dude = $resource_items;
            //\Drupal::config('resourcing.settings')->get('resource_default')
          }
          break;
        case "resource_date":
          $field_date = $value;
          break;
        case "resource_items":
          $resource_items = $value;
          $rows = explode("\r\n", $value);

          foreach ($rows AS $row) {
            $row_array = explode(":",$row);
            //label,duration,status,tag,body
            if (count($row_array) < 2) {
              unset($row_array);
            }
            if (!empty($row_array)) {
              $item = (object) [
                'type' => 'card',
                'title'=> $row_array[0],//required
                'field_duration' => $row_array[1],//required
                'uid' => $account->id(),
              ];
              if (!empty($row_array[2])) $item->field_status = $row_array[2];
              if (!empty($row_array[3])) $item->field_tags = $row_array[3];
              if (!empty($row_array[4])) $item->body = $row_array[4];
              $complete_nodes[] = $item;
            }
          }
          break;
        default:
          break;
      }
      //\Drupal::messenger()->addStatus($key . ': ' . $value);
    }
    if (!empty($complete_nodes)) {
      //set posted time manually to seed an initial order in the display
      $created_time = time();
      $count = 0;
      foreach ($complete_nodes AS $new_node) {
        $my_created_time = $created_time + $count;
        $count += 60;
        $new_node->created = $my_created_time;
        $new_node->field_date = $field_date;
        $node = Node::create((array)$new_node);
        $result = $node->save();
        \Drupal::messenger()->addStatus("Saved a Node.");
      }
    }

   }
}
