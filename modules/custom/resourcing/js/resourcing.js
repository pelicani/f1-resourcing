var data = {
  message: 'You loaded this page on ' + new Date().toLocaleString(),
  json_location: '/jsonapi/node/card?&filter[uid.id][value]=68b8c53e-3767-4c30-8002-33ebea273a9c&filter[status-filter][condition][path]=field_status&filter[status-filter][condition][value]=active',
  full_resource_list: [],
};

//list author uuids in drop down
//show categories on results
Vue.component(
  'resource-item',
  {
    props: ['card'],
    template: '<tr><td>{{ card.title }}</td><td>{{ card.field_date }}</td><td>{{ card.field_duration }}</td><td>{{ card.field_status }}</td></tr>'
  }
)

var redash = new Vue({
  delimiters : ['[[',']]'],
  el: '#resourcing_dashboard',
  data: data,
  created: function () {
    console.log('Vue instance was created');
  },
  methods:  {
    process_json: function(key,value) {
      //console.log(value.links);
      //drupal_internal__nid
      return_value = value.attributes;
      return_value.user = value.relationships.uid;
      return return_value;
    },
    getResourcing: function () {
      jQuery.getJSON(data.json_location,function(json){
        console.log(data.json_location);
        console.log(json.data);
        jQuery.each(json.data, function(key, value) {
          value = redash.process_json(key,value);
          data.full_resource_list.push(value);
        });
      }.bind(this));
    },
  },
  mounted: function () {
    console.log('Resourcing instance was mounted');
    var user, category, date;
    this.getResourcing(user, category, date);
  },
  updated: function () {
    console.log('Vue instance was updated')
  },
  destroyed: function () {
    console.log('Vue instance was destroyed')
  }
})
