var data = {
  user_links: [
    {uid:7,name:'Mom'},
    {uid:3,name:'Dad'},
    {uid:9,name:'Elyse'},
    {uid:10,name:'Annika'}
  ],
};

Vue.component(
  'user_link',
  {
    props: ['user'],
    template: '<li><a :href="\'/resourcing/reporting/calendar-heatmap?uid=\' + user.uid">{{ user.name }}</a></li>'
    //template: '<li><a href="#" @click="loadHeatmap(user.uid)">{{ user.name }}</a></li>'
  }
)


var fodash = new Vue({
  delimiters : ['[[',']]'],
  el: '#user_links',
  data: data,
  created: function () {
    console.log('Vue instance was created');
  },
  methods:  {
    loadHeatmap: function (uid) {
      console.log("dude: "+uid);
    }
  },
  mounted: function () {
    console.log('User List instance was mounted');
  },
  updated: function () {
    console.log('Vue instance was updated')
  },
  destroyed: function () {
    console.log('Vue instance was destroyed')
  }
})
