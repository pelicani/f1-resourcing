var data = {
  message: 'LII : 52 weeks in a year, 52 cards in a deck of playing cards. Coincidence?!',
  json_location: '/jsonapi/node/card?sort=-field_date&fields[node--card]=uid,title,field_date,field_tags,field_duration,status,focus_card,resource_card,field_card_sequence&fields[user--user]=name,mail&fields[field_tags]=name,tid,field_suit&include=uid,field_tags&filter[card-focus][value]=1&filter[card-focus][operator]==&filter[card-focus][path]=focus_card',
  card_list: [],
  blank_card_list: [],
  saved_cards: [],
  terms: []
};

//list author uuids in drop down
//show categories on results
Vue.component(
  'focus-item',
  {
    props: ['card'],
    template: '<tr><td>{{ card.title }}</td><td>{{ card.field_date }}</td><td>{{ card.field_tags }}</td><td>{{ card.field_card_sequence }}</td></tr>'
  }
)

var fodash = new Vue({
  delimiters : ['[[',']]'],
  el: '#focus_dashboard',
  data: data,
  created: function () {
    console.log('Vue instance was created');
  },
  methods:  {
    getFocus: function (user, year) {
      var saved_cards = [];
      var terms = [];
      jQuery.getJSON(data.json_location,function(json){
        //create included taxonomy relationships
        jQuery.each(json.included, function(index, object) {
          if (object.type.includes('taxonomy_term')) {
            term = {};
            term.id = object.id;
            term.name = object.attributes.name;
            term.suit = object.attributes.field_suit;
            terms.push(term);
          }
          if (object.type == 'user--user') {}
        });
        jQuery.each(json.data, function(key, value) {
          //value.attributes.card = cards[value.attributes.field_card_sequence];
          //find Suit Name
          terms.forEach(function(term){
            if (value.relationships.field_tags.data.id == term.id) {
              value.attributes.suit = term.suit;
            }
          });
          //switch date formatted
          reformat_date = value.attributes.field_date.split("-");
          value.attributes.field_date = reformat_date[1]+"-"+reformat_date[2]+"-"+reformat_date[0];
          saved_cards.push(value.attributes);
        });

        //define all cards
        var suits = ['Heart','Club','Diamond','Spade'];
        var cards = ['Ace','Two','Three','Four','Five','Six','Seven','Eight','Nine','Ten','Jack','Queen','King'];
        var cards_sort = ['Two','Three','Four','Five','Six','Seven','Eight','Nine','Ten','Jack','Queen','King','Ace'];
        cards_sort.forEach(function(card) {
          suits.forEach(function(suit) {
            display_card = {
              'title': suit + ":" + card,
              'suit': suit,
              'card': card,
              'date' : false,
            };
            data.blank_card_list.push(display_card);
          });
        });

        // Get the first Monday in the month
        var d = new Date(year, '0', '1');
        var year = d.getYear();
        var mondays = [];
        d.setDate(1);
        while (d.getDay() !== 1) {
          d.setDate(d.getDate() + 1);
        }
        // Get all the other Mondays in the month
        while (d.getYear() === year) {
          var pushDate = new Date(d.getTime());
          month_value = pushDate.getMonth()+1;
          month_value = month_value.toString();
          while (month_value.length < 2) {
            month_value = '0'+month_value;
          }
          day_value = pushDate.getDate().toString();
          while (day_value.length < 2) {
            day_value = '0'+day_value;
          }
          mondays.push(month_value + '-' + day_value + '-' + pushDate.getFullYear());
          d.setDate(d.getDate() + 7);
        }
        mondays.sort();

        mondays.forEach(function(monday, index){
          var display_card = "";
          blank_card = data.blank_card_list[index];
          var suit = blank_card.suit;
          var card = blank_card.card;
          saved_cards.forEach(function(saved_card){
            if (saved_card.field_date != null) {
              if (monday == saved_card.field_date) {
                display_card = {
                  'title': saved_card.title,
                  'field_date': saved_card.field_date,
                  'field_tags': saved_card.suit,
                  'field_card_sequence': cards[saved_card.field_card_sequence-1],
                };
              }
            }
          });
          if (display_card == "") {
            display_card = {
              'title': 'Add New Focus Card',
              'field_date': monday,
              'field_tags': blank_card.suit,
              'field_card_sequence': blank_card.card
            };
          }
          data.card_list.push(display_card);
        });
      });
    },
  },
  mounted: function () {
    console.log('Resourcing instance was mounted');
    var user;
    //defined in HTML template ... focus_year
    console.log("hd.mwc : focus year : "+focus_year);
    this.getFocus(user, focus_year);
    console.log("hd.mwc : dude");
    //console.log(data.card_list);
  },
  updated: function () {
    console.log('Vue instance was updated')
  },
  destroyed: function () {
    console.log('Vue instance was destroyed')
  }
})
