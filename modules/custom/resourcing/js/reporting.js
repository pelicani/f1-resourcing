Vue.component('calendarHeatmap', VueCalendarHeatmap.CalendarHeatmap);

var data = {
  today: new Date(),
  uid: jQuery('.user_id').attr('id'),
  json_location: '/rest/reporting/calendar-heatmap',
  values: [],
  range: ['dddddd', 'b43232', '997e38', '649632', '0e4d3b'],//'894366', '3b5a77', ],
  max: 16,
};

var rodash = new Vue({
  delimiters : ['[[',']]'],
  el: '#resourcing-reporting',
  data: data,
  created: function () {
    console.log('Vue instance was created');
  },
  methods:  {
    process_json: function(key,value) {
      //console.log(value);
      return {date:value.date,count:value.count};
    },
    getReporting: function () {
      data.json_location = data.json_location+"?uid="+data.uid;
      console.log("dude"+data.uid);
      console.log(data.json_location);
      jQuery.getJSON(data.json_location,function(json){
        console.log(data.json_location);
        console.log(json);
        jQuery.each(json, function(key, value) {
          value = rodash.process_json(key,value);
          data.values.push(value);
        });
      }.bind(this));
    },
  },
  mounted: function () {
    console.log('Resourcing instance was mounted');
    this.getReporting();
  },
  updated: function () {
    console.log('Vue instance was updated')
  },
  destroyed: function () {
    console.log('Vue instance was destroyed')
  }
})
