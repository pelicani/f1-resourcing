//list author uuids in drop down
//show categories on results
Vue.component('animated-item', {
  props: ['item'],
  template: '<li class="list-item"><h2>{{ item.word }} one another</h2></li>'
})
var data = {
  active_one: '',
  message: 'one another',
  word_list: [
    {'id':0,'word':'encourage','isActive':false},
    {'id':1,'word':'bear with','isActive':false},
    {'id':2,'word':'regard as more important','isActive':false},
    {'id':3,'word':'greet','isActive':false},
    {'id':4,'word':'pray for','isActive':false},
    {'id':5,'word':'serve','isActive':false},
    {'id':6,'word':'accept','isActive':false},
    {'id':7,'word':'admonish','isActive':false},
    {'id':8,'word':'forgive','isActive':false},
    {'id':9,'word':'love','isActive':false}
  ],
};

var oadash = new Vue({
  delimiters : ['[[',']]'],
  el: '#one_another_dashboard',
  data: data,
  created: function () {
    console.log('Vue instance was created');
  },
  methods:  {
    pick_another: function() {
      random_int = data.active_one;
      if (data.active_one !== '') {
        for (let i = 0; i < data.word_list.length; i++) {
          data.word_list[i].isActive = false;
        }
      }
      while (random_int === data.active_one) {
        random_int = Math.floor(Math.random() * data.word_list.length);
      }
      data.active_one = random_int;
      data.word_list[random_int].isActive = true;

    }
  },
  mounted: function () {
    console.log('Resourcing instance was mounted');
    this.pick_another();
  },
  updated: function () {
    console.log('Vue instance was updated')
  },
  destroyed: function () {
    console.log('Vue instance was destroyed')
  }
})
