<?php

namespace Drupal\one_another\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class OneAnotherDashboardController.
 */
class OneAnotherDashboardController extends ControllerBase {

  /**
   * Dashboard.
   *
   * @return string
   *   Return Hello string.
   */
  public function dashboard() {
    return [
      '#theme' => 'oa_dashboard',
      '#intro' => 'Write some good stuff here.',
      '#attached' => [
        'library' => [
          'one_another/vuejs',
          'one_another/one_another',
        ],
      ]
    ];
  }

}
