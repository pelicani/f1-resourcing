# Webpack Babel

Provides Babel configuration for the webpack module.

### Dependencies

- The [Webpack](https://drupal.org/project/webpack) module.

### Installation

- `yarn add file:./web/modules/contrib/webpack_babel`

### Works great with

- [Webpack React](https://drupal.org/project/webpack_react)
- [Webpack Vue.js](https://drupal.org/project/webpack_vuejs)
