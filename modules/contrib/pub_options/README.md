# Publishing options module README.md

## CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

## INTRODUCTION
The publishing options module allows you to create your own custom promotion options.

## REQUIREMENTS
This module requires no modules outside of Drupal core.

## INSTALLATION
 - Install the Publishing options module as you would normally install a contributed Drupal
   module. Visit https://www.drupal.org/node/1897420 for further information.

## CONFIGURATION
To administer publishing options, go to `/admin/config/content/publishing-options`

#### Content types
After adding the desired amount of options, you can then go to `/admin/structure/types` and choose publishing
options for the desired content types.

Once a content type has the desired publishing options, you can select the publishing options available to the content
type.

#### Views
You can add a field, filter and contextual filter to a view for publishing options.

----------------------------------------------------------------------
###### Author/Maintainers
 - Jorge Calderon - https://www.drupal.org/u/geocalleo
