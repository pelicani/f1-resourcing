<?php

namespace Drupal\KernelTests\Core\File;

use Drupal\KernelTests\KernelTestBase;
use Symfony\Component\Process\PhpExecutableFinder;
use Symfony\Component\Process\Process;

/**
 * Tests that the phar stream wrapper works.
 *
 * @group File
 */
class PharWrapperTest extends KernelTestBase {

  /**
   * Tests that only valid phar files can be used.
   */
  public function testPharFile() {
    $base = $this->getDrupalRoot() . '/core/tests/fixtures/files';
    // Ensure that file operations via the phar:// stream wrapper work for phar
    // files with the .phar extension.
    $this->assertFileNotExists("phar://$base/phar-1.phar/no-such-file.php");
    $this->assertFileExists("phar://$base/phar-1.phar/index.php");
    $file_contents = file_get_contents("phar://$base/phar-1.phar/index.php");
    $expected_hash = 'c7e7904ea573c5ebea3ef00bb08c1f86af1a45961fbfbeb1892ff4a98fd73ad5';
    $this->assertSame($expected_hash, hash('sha256', $file_contents));

    // Ensure that phar aliases work.
    include __DIR__ . '/fixtures/alias_phar.phar';
    $this->assertSame("Included a file inside the phar using 'phar://alias_phar.phar/test/file_to_include.php'", phar_test_include());

    // Ensure that file operations via the phar:// stream wrapper throw an
    // exception for files without the .phar extension.
    $this->expectException('TYPO3\PharStreamWrapper\Exception');
    file_exists("phar://$base/image-2.jpg/index.php");
  }

  /**
   * Tests phar files not ending in .phar can be executed using the CLI.
   */
  public function testCliPharFile() {
    $php = (new PhpExecutableFinder())->find();
    $process = new Process("$php cli_phar", __DIR__ . '/fixtures');
    $process->setTimeout(0);
    $process->run();
    $expected_output = <<<EOF
Can access phar files without .phar extension if they are the CLI command.
Can access phar files with .phar extension.
Cannot access other phar files without .phar extension.
Included a file inside the phar using 'phar://cli_phar/test/file_to_include.php'.
Shutdown functions work in phar files without a .phar extension.
Shutdown functions cannot access other phar files without .phar extension.

EOF;

    $this->assertSame($expected_output, $process->getOutput());
    $this->assertSame(0, $process->getExitCode());
  }

}
